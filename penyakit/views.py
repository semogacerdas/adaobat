from django.shortcuts import render, HttpResponse
from penyakit.models import PenyakitSementara
import csv
import random

# Create your views here.
def get_random_penyakit():
    n = random.randint(1,20)
    return PenyakitSementara.objects.get(id=n)

def hill_climbing_search_sementara(list_of_disease):
    obj = get_random_penyakit()
    penyakit_jawaban = hill_climbing_recursive_sementara(obj,list_of_disease)
    for i in range(4):
        obj = get_random_penyakit()
        penyekit_sementara = hill_climbing_recursive_sementara(obj,list_of_disease)
        if bandingin_penyakit(penyekit_sementara,penyakit_jawaban,list_of_disease):
            penyakit_jawaban = penyekit_sementara
    return penyakit_jawaban

def bandingin_penyakit(penyakit1, penyakit2, list_of_disease):
    if penyakit1.find_heuristic_value(list_of_disease)[0] > penyakit2.find_heuristic_value(list_of_disease)[0]:
        return True
    elif penyakit1.find_heuristic_value(list_of_disease)[0] == penyakit2.find_heuristic_value(list_of_disease)[0]:
        if penyakit1.find_heuristic_value(list_of_disease)[1] > penyakit2.find_heuristic_value(list_of_disease)[1]:
            return True
    return False
    
def hill_climbing_recursive_sementara(penyakit,list_of_disease):
    print("state : "+str(penyakit.nama_penyakit))
    penyakit_tertinggi = penyakit
    for neighbour in penyakit.neighbours.all():
        if bandingin_penyakit(neighbour,penyakit_tertinggi,list_of_disease):
            penyakit_tertinggi = neighbour

    if penyakit_tertinggi==penyakit:
        return penyakit
    else:
        return hill_climbing_recursive_sementara(penyakit_tertinggi,list_of_disease)
