import csv

from django.db.utils import IntegrityError
from django.core.exceptions import FieldDoesNotExist
from django.db import transaction


class CsvUploader:

    def __init__(self, csv_file, model):
        self.model = model
        self.reader = list(csv.DictReader(csv_file, delimiter=','))
        self.keys = [k for k in self.reader[0]]
        self.model_fields = [f.name for f in self.model._meta.get_fields()]
        self.valid = self._validate_csv()
        self.csv_pos = 0

    def _validate_csv(self):

        keys = []
        for k in self.keys:
            if k.endswith("_id"):
                keys.append(k[:-3])
            else:
                keys.append(k)
        return set(keys).issubset(self.model_fields)

    def read_chunk(self):
        chunk = []
        for i in range(1000):
            try:
                chunk.append(self.model(**self.reader[self.csv_pos]))
            except IndexError as e:
                print(e)
                break
            self.csv_pos += 1
        return chunk

    def create_records(self):

        if not self.valid:
            return "Invalid csv file"

        while True:
            chunk = self.read_chunk()

            if not chunk:
                break

            try:
                with transaction.atomic():
                    self.model.objects.bulk_create(chunk)
            except IntegrityError as e:
                for i in chunk:
                    try:
                        i.save()
                    except IntegrityError:
                        continue
                print("Exeption: {}".format(e))

            return "penyakit succesfully saved!"
