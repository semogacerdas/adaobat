from django.db import models

# Create your models here.
# class PenyakitManager(models.Manager):
#     def create_penyakit(self,nama_penyakit,nilai_probabilitas):
#         penyakit = self.create(nama_penyakit=nama_penyakit,nilai_probabilitas=nilai_probabilitas)
#         list_penyakit = Penyakit.objects.all()
#         for m_penyakit in list_penyakit:
#             m_penyakit.neighbours.add(penyakit)
#         return penyakit

class PenyakitSementara(models.Model):
    nama_penyakit = models.CharField(
        max_length = 100
    )
    symptom_1 = models.CharField(
        max_length = 100
    )
    symptom_2 = models.CharField(
        max_length = 100
    )
    symptom_3 = models.CharField(
        max_length = 100
    )
    symptom_4 = models.CharField(
        max_length = 100
    )
    symptom_5 = models.CharField(
        max_length = 100,
        blank=True,
        null=True
    )
    neighbours = models.ManyToManyField(
        'self',
        related_name="neighbours_s",
        blank=True
    )

    def find_heuristic_value(self, listsOfDisease):
        value = 0
        symptomps_match = 0
        for i in listsOfDisease:
            if self.symptom_1 == ' '+i:
                symptomps_match+=1
                value+=5
            elif self.symptom_2 == ' '+i:
                symptomps_match+=1
                value+=4
            elif self.symptom_3 == ' '+i:
                value+=3
                symptomps_match+=1
            elif self.symptom_4 == ' '+i:
                value+=2
                symptomps_match+=1
            elif self.symptom_5 == ' '+i:
                value+=1
                symptomps_match+=1
        return [symptomps_match, value]

class Gejala(models.Model):
    nama_inggris = models.CharField(
        max_length = 100
    )
    nama_indo = models.CharField(
        max_length = 100
    )
    kode_gejala = models.CharField(
        max_length = 100
    )