from django.contrib import admin, messages
# from django.shortcuts import redirect
# from django import forms
# from django.urls import path

# from .csv_uploader import CsvUploader
# from .models import Penyakit
# import io
# import csv
# # Register your models here.


# class CsvUploadForm(forms.Form):
#     csv_file = forms.FileField()


# class CsvUploadAdmin(admin.ModelAdmin):

#     change_list_template = "csv_form.html"

#     def get_urls(self):
#         urls = super().get_urls()
#         additional_urls = [
#             path("upload-csv/", self.upload_csv),
#         ]
#         return additional_urls + urls

#     def changelist_view(self, request, extra_context=None):
#         extra = extra_context or {}
#         extra["csv_upload_form"] = CsvUploadForm()
#         return super(CsvUploadAdmin, self).changelist_view(request, extra_context=extra)

#     def upload_csv(self, request):
#         if request.method == "POST":
#             form = CsvUploadForm(request.POST, request.FILES)
#             if form.is_valid():
#                 if request.FILES['csv_file'].name.endswith('csv'):

#                     try:
#                         decoded_file = request.FILES['csv_file'].read().decode(
#                             'utf-8')
#                     except UnicodeDecodeError as e:
#                         self.message_user(
#                             request,
#                             "There was an error decoding the file:{}".format(
#                                 e),
#                             level=messages.ERROR
#                         )
#                         return redirect("..")

#                     # print(decoded_file)
#                     io_string = io.StringIO(decoded_file)
#                     # print(io_string)
#                     # uploader = CsvUploader(io_string, self.model)
#                     # uploader.create_records()
#                     next(io_string)
#                     for column in csv.reader(io_string, delimiter=',', quotechar="|"):
#                         try:
#                             _, created = Penyakit.objects.update_or_create(
#                                 nama_penyakit=column[1],
#                                 symptom_1=column[2],
#                                 symptom_2=column[3],
#                                 symptom_3=column[4],
#                                 symptom_4=column[5],
#                                 symptom_5=column[6],
#                             )
#                             neighbours = str(column[7])
#                             current_penyakit = Penyakit.objects.get(
#                                 nama_penyakit=column[1])
#                             if neighbours != "":
#                                 for i in list(neighbours.split(' ')):
#                                     if i.isdigit():
#                                         print(i)
#                                         # crr_neighbour = Penyakit.objects.get(
#                                         #     pk=i)
#                                         # current_penyakit.neighbours.add(
#                                         #     crr_neighbour)
#                                         # current_penyakit.save

#                         except IndexError:
#                             self.message_user(request, "kepanjangan")

#                 else:
#                     self.message_user(request, "salah file type")

#             else:
#                 self.message_user(request, "there was an error in the form")

#         return redirect("..")

#     # def upload_csv(self, request):
#     #     if request.method == "POST":

#     #         # Here we will process the csv file

# #     #     return redirect()


# @admin.register(Penyakit)
# class PenyakitAdmin(CsvUploadAdmin):
#     pass
