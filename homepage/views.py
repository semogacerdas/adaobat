from django.shortcuts import render
from penyakit.views import hill_climbing_search_sementara
from penyakit.models import Gejala, PenyakitSementara
from obat.views import get_obat_from_penyakit
from obat.models import Obat

def index(request):
    return render(request, 'index.html')

def input(request):
    SYMPTOMS = Gejala.objects.all()
    context = {'symptoms': SYMPTOMS}
    if (request.method == 'POST'):
        list_of_symptoms = []
        for key in request.POST:
            list_of_symptoms.append(request.POST[key])
            print(request.POST[key])
        context['penyakit'] = hill_climbing_search_sementara(list_of_symptoms).nama_penyakit
        return show_obat(request, context['penyakit'])
    return render(request, 'input.html', context)

def show_obat(request, penyakit):
    id_penyakit = PenyakitSementara.objects.filter(nama_penyakit=penyakit).get().id
    obat = get_obat_from_penyakit(id_penyakit)
    context = {
        'penyakit': penyakit,
        'obats': obat
    }
    return render(request, 'result.html', context)

def tentang(request):
    return render(request, 'tentang.html')