from django.shortcuts import render
from .models import Obat
from django.core.management import call_command
from django.http import HttpResponse


# Create your views here.


def get_obat_from_penyakit(id_penyakit):
    queryset_obat = Obat.objects.filter(penyakit__id=id_penyakit)
    for obat in queryset_obat:
        print(obat.nama_obat)
    return queryset_obat

# Buat panggil obatnya, panggil method get_obat_from_penyakit parameternya penyakit.id . Nanti dapet queryset, bisa di loop. Penyakitnya diambil dari views 
# aplikasi penyakit pake hill_climbing_search_sementara(list_desease).

def create_database(request):
    call_command('create_data')
    html = "<html><body>dataIsCreated</body></html>"
    return HttpResponse(html)
    return 