from django.db import models
from penyakit.views import PenyakitSementara

# Create your models here.
class Obat(models.Model):
    nama_obat = models.CharField(max_length = 100)
    deskripsi = models.TextField()
    penyakit = models.ForeignKey(PenyakitSementara, on_delete=models.CASCADE)
